from argparse import ArgumentParser


__author__ = 'ikrasner'

default_file_path = './input.txt'


def check_word_start(line, start_position, word_len):
    if start_position == 0:
        return True
    if not line[start_position-1].isalpha():
        return True
    return False


def check_word_end(string, start_position, word_len):

    if start_position + word_len == len(string):
        return True
    if not string[start_position+word_len].isalpha():
        return True
    return False


def is_word(string, start_position, word_len):
    return check_word_end(string, start_position, word_len) and check_word_start(string, start_position, word_len)


def find_all_words_in_string(string, word):
    words_count = 0
    pos = string.lower().find(word.lower())
    string_position = 0
    while pos != -1:
        if is_word(string, string_position+pos, len(word)):
            words_count += 1
        string_position = string_position + pos + len(word)+1
        pos = string[string_position:].find(word)
    return words_count


if __name__ == "__main__":

    parser = ArgumentParser(description="Counts word occurrence in text file. File should be in UTF8 encoding")
    parser.add_argument('-f', '--file-path', dest='file_path', type=str, default=default_file_path)
    parser.add_argument('-w', '--word', dest='word', type=str)
    args = parser.parse_args()
    decoded_word = args.word
    word_counter = 0
    with open(args.file_path, "rt", encoding="utf8") as input_file:
        for line in input_file:
            res = find_all_words_in_string(line.strip(), decoded_word)
            word_counter += res
    print(word_counter)


