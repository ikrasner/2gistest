from AttendanceCalculator.configs import default_db_file

__author__ = 'ikrasner'
import sqlite3

db_file = default_db_file


def get_connection():
    return sqlite3.connect(db_file)


def exec_sql(sql):
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    conn.close()


def select(sql):
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()
    conn.close()
    return res


def insert(sql):
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    conn.close()
