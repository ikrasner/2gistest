from datetime import datetime
from dateutil.parser import parserinfo
from delorean.dates import Delorean

__author__ = 'ikrasner'

unix = Delorean(datetime(1970, 1, 1), timezone='UTC')
date_format = "%d-%m-%Y %H:%M:%S"
default_input_file_path = "./data.xml"
my_timezone = "Asia/Novosibirsk"
p_info = parserinfo(dayfirst=True, yearfirst=False)
default_db_file = "./data.db"

