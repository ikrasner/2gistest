from argparse import ArgumentParser
from datetime import datetime, timedelta
import os

from dateutil.parser import parse
from delorean.dates import Delorean
from lxml import etree
import sys


from AttendanceCalculator import DBConnector
from AttendanceCalculator.DBConnector import exec_sql, insert, select
from AttendanceCalculator.configs import unix, date_format, my_timezone, default_input_file_path, p_info, \
    default_db_file

__author__ = 'ikrasner'


def get_unix_timestamp(some_date):
    return int((some_date-unix).total_seconds())


def get_attendance(name, period_start_unix=0, period_end_unix=sys.maxsize):
    user_id = find_user(name)
    if not user_id:
        return None
    periods = select("select start, end from attendance where user_id = %d and end>%d and start<%d order by start asc" %
                     (user_id, period_start_unix, period_end_unix))
    if not periods:
        return 0
    s, e = periods[0]
    periods[0] = (s, e) if period_start_unix < s else (period_start_unix, e)
    s, e = periods[-1]
    periods[-1] = (s, e) if period_end_unix > e else (s, period_end_unix)

    total_time = 0
    for ps, pe in periods:
        total_time += pe-ps
    return total_time


def create_schema():
    exec_sql("create table users (id integer primary key autoincrement, name varchar(255) unique)")
    exec_sql("create table attendance (user_id integer, start integer, end integer)")


def find_user(name):
    res = select("select id from users where name = '%s'" % name)
    if not res:
        return None
    return res[0][0]


def add_user(name):
    res = insert("insert into users (name) values ('%s')" % name)


def add_attendance(name, start, end):
    user_id = find_user(name)
    insert("insert into attendance (user_id, start, end) values (%d, %d, %d)" % (user_id, start, end))


def import_xml_to_db(file_path):
    tree = etree.parse(file_path)
    res = tree.xpath('/peoples/people')
    processed_users = []
    for r in res:
        name = r.xpath('name')[0].text
        start = datetime.strptime(r.xpath('start')[0].text, date_format)
        end = datetime.strptime(r.xpath('end')[0].text, date_format)
        start_date = Delorean(datetime=start, timezone=my_timezone)
        end_date = Delorean(datetime=end, timezone=my_timezone)
        if name not in processed_users:
            add_user(name)
            processed_users.append(name)
        add_attendance(name, get_unix_timestamp(start_date), get_unix_timestamp(end_date))

if __name__ == "__main__":
    parser = ArgumentParser(description="Load user attendance to database. Get user attendance for period."
                                        "Every time database will be rewritten")
    parser.add_argument('-i', '--input-file', dest='input_path', type=str, default=default_input_file_path,
                        help="Path to XML file")
    parser.add_argument('-d', '--database', dest='db_file', type=str, default=default_db_file,
                        help="Path to database file file")

    parser.add_argument('-s', '--start-date', dest='period_start', type=str,
                        help="If not set attendance is calculated from the very beginning")
    parser.add_argument('-e', '--end-date', dest='period_end', type=str,
                        help="If not set attendance is calculated to the last record")
    parser.add_argument('-n', '--name', dest='name', type=str, help="User name")

    parser.add_argument('-r', action='store_true', help="If set show results in human readable format")

    args = parser.parse_args()
    DBConnector.db_file = args.db_file
    try:
        os.remove(args.db_file)
    except OSError as e:
        print(e)
        print(type(e))
    create_schema()
    import_xml_to_db(args.input_path)
    if args.name is not None:
        user_name = args.name
        params = {'name': user_name}
        if args.period_start is not None:
            period_start_date = Delorean(parse(args.period_start, p_info), timezone=my_timezone)
            params['period_start_unix'] = get_unix_timestamp(period_start_date)

        if args.period_end is not None:
            period_end_date = Delorean(parse(args.period_end, p_info), timezone=my_timezone)
            params['period_end_unix'] = get_unix_timestamp(period_end_date)

        res = get_attendance(**params)
        if res is None:
            print("User %s is not found" % user_name)
            exit(1)
        print(str(timedelta(seconds=res)) if args.r else res)


